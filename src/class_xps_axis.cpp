#include <stdio.h> 
#include <stdlib.h> 
#include <stdarg.h> 
#include <string.h>

#include <iostream>

#include "class_xps_axis.h"

#include "XPS_lib.h"
#include "Exception.h"

/**
 * the classical format for a communication port is an unsigned short  
 */
typedef unsigned short u_short;

/**
 * XPS is listening for commands on port 5001  
 */
u_short port_XPS = 5001;

/**
 * We have to Create a TCP/IP connection (using ACE instead of native socket)<BR>
 * We'll use two socket. One sends commands and wait for the response but the <BR>
 * other one is just a "send socket"... See more details in class_xps_axis.h
 */
int XPS_axis::create_dialog()
{
  int code;
  /**
   * connector has the capabilitie to establishe a new end-point of communication.  
   */
  ACE_SOCK_Connector connector;
  /**
   *Addr_XPS constructor converts the port number and ip address to network byte order. 
   */
  ACE_INET_Addr addr_XPS(port_XPS,ip_addr);
  /**
   *socket_send_XPS is an ACE_SOCK_Stream: So we initiate a connection and then<BR>
   *we initialize an ACE_SOCK_Stream after the connection is established. <BR>
   *if everything runs fine we'll be able to communicate.
   */
  code=connector.connect (socket_send_XPS,addr_XPS);
  /**
   *Ace natively manage errors with a standard error "C" return code<BR>
   *-1 = error <BR>
   */
  if (code == -1)
  {
    /**
     *we use 2 mechanism to announce the error<BR>
     *The ace native one and the exception mechanism that "soleil" provides
     *We keep the native ACE error mangement for debugging purpose
     */
    ACE_ERROR_RETURN ((LM_ERROR, "%p\n", "open"),-1); 
    throw thirdparty::Exception(std::string("COMMUNICATION_ERROR"),// reason
                                std::string("ACE SOCKET FATAL ERROR WHILE CONNECTING"),// description
                                std::string("XPS_axis::create_dialog"),// origine
                                thirdparty::ERR);//severity

  }
  else
  {   
    /**
     * The same job for our other socket...  
     */
    code=connector.connect (socket_send_and_receive_XPS,addr_XPS);//ok? --> let's go to connect our other ACE_SOCK_Stream
    if (code== -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR,"%p\n","open"),-1);//if something wrong
      throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE CONNECTING"),// reason
                                  std::string("ACE SOCKET FATAL ERROR"),// description
                                  std::string("XPS_axis::create_dialog"),// origine
                                  thirdparty::ERR);//severity
    }
  }
  //- interdit la com si MC pas atteignable
  this->m_is_connected = true;
  return(code);
}

/**
 *    To close a TCP/IACE_Time_Value timeout (0,10000);P connection 
 */
int XPS_axis::close_dialog()
{
  int code;
  this->m_is_connected = false;
  ACE_Time_Value t (0,100000);//t=100ms
  code=socket_send_XPS.close ();//close the connection
  if (code == -1)//check for error
  {
    ACE_ERROR_RETURN ((LM_ERROR,"%p\n","close"),-1);//if something wrong while closing the dialog say it! 
    throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE DISCONNECTING"),// reason
                                std::string("ACE SOCKET FATAL ERROR"),// description
                                std::string("XPS_axis::close_dialog"),// origine
                                thirdparty::ERR);//severity
  }
  else
  {
    ACE_OS::sleep(t); // wait 100 ms when close (just to give some time for the XPS stack)
    code=socket_send_and_receive_XPS.close();//close the other connection
    if (code == -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR, "%p\n", "close"),-1);
      throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE DISCONNECTING"),// reason
                                  std::string("ACE SOCKET FATAL ERROR"),// description
                                  std::string("XPS_axis::close_dialog"),// origine
                                  thirdparty::ERR);//severity
    }
    else
      ACE_OS::sleep(t); // wait 100 ms when close
  }
  return(code);
}


/*-----------------------------------------------------------------------------*/
/*     To send a message to XPS WITHOUT taking care for the answer         */
/*-----------------------------------------------------------------------------*/ 
int XPS_axis::send (char *message)
{
  char buf[SIZE_BUFFER];//this one,
  char * reponse;//this one, 
  reponse=buf;//and this one are tricks to catch the answer without treating it 
  int code;
  ACE_Time_Value timeout (0,10000); // time out 10 ms
  //- interdit la com si pas de connection
  if(!this->m_is_connected)
    return -1;

  code=socket_send_XPS.send_n (message,ACE_OS::strlen (message));//send the message
  if (code == -1)//check fo error
  {
    m_is_connected = ++nb_timeouts < nb_max_timeouts;
    ACE_ERROR_RETURN ((LM_ERROR, "%p\n","send"),-1);
    throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE SENDING A PACKET"),// reason
                                std::string("ACE SOCKET FATAL ERROR"),// description
                                std::string("XPS_axis::send"),// origine
                                thirdparty::ERR);//severity
  }
  else
    socket_send_XPS.recv (reponse,SIZE_BUFFER,&timeout);//wait 10 ms for the answer and do nothing
  //don't need to check for a timeout message
  //while anyway we'll do nothing.
  nb_timeouts = 0;
  return (code);
}

/*-------------------------------------------------------------------------------------*/
/*     To send a message to XPS TAKING care for the answer (for treatment)         */
/*-------------------------------------------------------------------------------------*/
int XPS_axis::send_and_receive (char *message, char *reponse)
{
  int code;

  //flush the reponse array (reponse = answers of xps)
  ::memset(reponse, 0L, SIZE_BUFFER);

  //- interdit la com si pas de connection
  if(!this->m_is_connected)
    return -1;

  code = socket_send_and_receive_XPS.send_n (message,ACE_OS::strlen (message));//send the message
  if (code == -1)//check for error
  {
    ACE_ERROR_RETURN ((LM_ERROR,"%p\n","send"),-1);//if something wrong while sending the message say it!
    throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE SENDING A PACKET"),// reason
                                std::string("ACE SOCKET FATAL ERROR"),// description
                                std::string("XPS_axis::send_and_receive"),// origine
                                thirdparty::ERR);//severity
  }
  else
  {
    code=socket_send_and_receive_XPS.recv(reponse,SIZE_BUFFER,&timeout);//Waiting for an answer
    if (code == -1)//check for error
    {
      if (ACE_OS::last_error() == ETIME)//if the error is a timeout
      {
        m_is_connected = ++nb_timeouts < nb_max_timeouts;
        ACE_DEBUG ((LM_DEBUG,ACE_TEXT ("(%p|%t) Timeout while ") ACE_TEXT ("receiving\n")));//say it
        throw thirdparty::Exception(std::string("TIME OUT WHILE WAITING FOR A REPONSE"),// reason
                                    std::string("TIME OUT"),// description
                                    std::string("XPS_axis::send_and_receive"),// origine
                                    thirdparty::ERR);//severity
      }
      else
      {
        m_is_connected = ++nb_timeouts < nb_max_timeouts;
        ACE_ERROR_RETURN ((LM_ERROR,"%p\n","received"),-1);//if the error is something else say what.
        throw thirdparty::Exception(std::string("COMMUNICATION_ERROR WHILE RECEIVING A PACKET"),// reason
                                    std::string("ACE SOCKET FATAL ERROR"),// description
                                    std::string("XPS_axis::send_and_receive"),// origine
                                    thirdparty::ERR);//severity
      }
    }
  }
  nb_timeouts = 0;
  return (code);
}

/*---------------------------------------------*/
/*        to initialize time out value         */
/* for "send_and_receive" function             */
/*---------------------------------------------*/

int XPS_axis::set_timeout(ACE_Time_Value time)
{
  timeout=time;
  return(0);
}

/*---------------------------------------------*/
/*        to read time out value               */
/* for "send_and_receive" function             */
/*---------------------------------------------*/

int XPS_axis::get_timeout(ACE_Time_Value &time)
{
  time=timeout;
  return(0);
}

/*----------------------------------------------*/
/*     all these functions refer to XPS_lib     */
/*   wich is our "interface" between            */
/*     specs protocol and XPS protocol          */
/*----------------------------------------------*/

int XPS_axis::position_read(double &pos)
{
  int code;
  code=GroupPositionCurrentGet(this,1,&pos);//(remember this => group.axe) 1 is because we use only 1 axis by group
  //pos is the position readed
  sprintf(origin_error,"XPS_axis::position_read");//in case of error we can get the origin of the error
  test_error(code);//to make the link between the XPS error code and the tango exception management.
  return(code);//We keep it for debuging purpose
}

int XPS_axis::get_speed(double &velocity)
{
  int code;
  double MinimumTjerkTime;
  double MaximumTjerkTime;
  code=PositionerSGammaParametersGet (this,&speed,&acceleration,&MinimumTjerkTime,&MaximumTjerkTime);//send the command 
  velocity=speed;
  sprintf(origin_error,"Get Speed");
  test_error(code);
  return(code);
}

int XPS_axis::set_speed(double &velocity)
{
  int code;
  double MinimumTjerkTime;
  double MaximumTjerkTime;
  code=PositionerSGammaParametersGet (this,&speed,&acceleration,&MinimumTjerkTime,&MaximumTjerkTime); 
  if (code==0)
  {
    code=PositionerSGammaParametersSet (this,velocity,acceleration,MinimumTjerkTime,MaximumTjerkTime);
    if (code==0)
      speed=velocity;
  }
  sprintf(origin_error,"Set Speed");
  test_error(code);
  return(code);
}

int XPS_axis::get_acc(double &acc)
{
  int code;
  double MinimumTjerkTime;
  double MaximumTjerkTime;
  code=PositionerSGammaParametersGet (this,&speed,&acceleration,&MinimumTjerkTime,&MaximumTjerkTime); 
  if (code==0)
  {
    acc=acceleration;
  }
  sprintf(origin_error,"Get Acceleration");
  test_error(code);
  return(code);
}               

int XPS_axis::set_acc(double &acc)
{
  int code;
  double MinimumTjerkTime;
  double MaximumTjerkTime;
  acceleration=acc;
  code=PositionerSGammaParametersGet (this,&speed,&acceleration,&MinimumTjerkTime,&MaximumTjerkTime); 
  if (code==0)
  {
    code=PositionerSGammaParametersSet (this,speed,acc,MinimumTjerkTime,MaximumTjerkTime); 
    if (code==0)
    {
      acceleration=acc;
    }
  }
  sprintf(origin_error,"Set Acceleration");
  test_error(code);
  return(code);
}               

int XPS_axis::is_initialized(bool &init)
{
  int status;
  int code;
  code=GroupStatusGet (this,&status);
  init=(((status>9)&&(status<40))||((status>43)&&(status<50)));
  sprintf(origin_error,"Get Status for initialized");
  test_error(code);
  return(code);
}

int XPS_axis::is_ready(bool &ready)
{
  int status;
  int code;
  code=GroupStatusGet (this,&status);
  ready=((status>9)&&(status<20));
  sprintf(origin_error,"Get Status for ready");
  test_error(code);
  return(code);
}

int XPS_axis::is_motor_on(bool &is_on)
{
  int status;
  int code;
  code=GroupStatusGet (this,&status);
  is_on=(((status>9)&&(status<20))||((status>42)&&(status<50)));
  sprintf(origin_error,"Get Status for motor on");
  test_error(code);
  return(code);
}

int XPS_axis::is_moving(bool &mouve)
{
  int status;
  int code;
  double vit;
  double acc;
  code=GroupStatusGet (this,&status);
  if (code==0)
  {
    if (status==47) 
    {
      code=GroupJogCurrentGet(this,1,&vit,&acc);
      mouve=((vit > 0.00001)||(vit<-0.00001));
    }
    else
    {
      mouve=(status>42);
    }
  }
  sprintf(origin_error,"Get Status for moving");
  test_error(code);
  return(code);
}


// limit switches   
int XPS_axis::read_eor(int &eor)
{
  int code;
  int HardwareStatus;
  code=PositionerHardwareStatusGet (this,&HardwareStatus); 
  if (code==0)
  {
    HardwareStatus=(HardwareStatus >> 8);
    HardwareStatus=(HardwareStatus & 3);
  }
  eor=HardwareStatus;;
  sprintf(origin_error,"Read end of run");
  test_error(code);
  return(code);
}

int XPS_axis::read_status(int &stat)
{
  int code;
  code=GroupStatusGet (this,&stat);
  sprintf(origin_error,"Get Status");
  test_error(code);
  return(code);
}

int XPS_axis::set_motor_on()
{
  int code;
  code=GroupMotionEnable(this); 
  sprintf(origin_error,"Set motor ON");
  test_error(code);
  return(code);
}

int XPS_axis::set_motor_off()
{
  int code; 
  int status;
  code=GroupStatusGet(this,&status);
  if((status==44) ||(status==47) ) // JOG / PoSITIONNING
  {
    stop ();
  }
  code=GroupMotionDisable(this); 
  sprintf(origin_error,"Set motor OFF");
  test_error(code);
  return(code);
}

int XPS_axis::initialization()
{
  int code;
  GroupKill(this);
  GroupInitialize (this);
  code=GroupHomeSearch(this);
  sprintf(origin_error,"Homing");
  test_error(code);
  return(code);
}

//------------------------------------------------------------------
//- define position :
// to set the current axis postion to the position
//------------------------------------------------------------------
int XPS_axis::define_position(double position)
{
  int code;
  
  GroupKill(this);
  GroupInitialize(this);

  code = GroupReferencingStart (this) ;
  if (code)
  {
    sprintf(origin_error,"Define Position on GroupReferencingStart");
    test_error(code);
    return(code);
  }
  
  //- ReferencingAction can be a lot of stuff, ReferencingSensor  also...
  code = GroupReferencingActionExecute (this, "SetPosition", "None", position);
  if (code)
  {
    sprintf(origin_error,"Define Position on GroupReferencingActionExecute");
    test_error(code);
    return(code);
  }

  code = GroupReferencingStop (this);
  	
  sprintf(origin_error,"Define Position on GroupReferencingStop");
  test_error(code);
  return(code);
}

  
      

int XPS_axis::move_absolute(double pos)
{
  int code;
  int status;
  code=GroupStatusGet (this,&status);
  if (code==0)
  {
    if ((status>9)&&(status<20))
    {
      code=PositionerMoveAbsolute (this, pos);
    }
    else
    {
      code = -22;
    }
  }
  sprintf(origin_error,"Absolute displacement");
  test_error(code);
  return(code);
}


int XPS_axis::move_relative(double depl)
{
  int code;
  int status;
  code=GroupStatusGet (this,&status);
  if (code==0)
  {
    if ((status>9)&&(status<20))
    {
      code=GroupMoveRelative (this,1,&depl);
    }
    else
    {
      code=-22;
    }
  }
  sprintf(origin_error,"Relative displacement");
  test_error(code);
  return(code);
}

int XPS_axis::end_jog()
{
  int code;
  double vitesse;
  double acc;
  int status;
  ACE_Time_Value t (0,100000);// t=100ms
  code=GroupStatusGet (this,&status);
  if (code==0) 
  {
    if (status!=47)
    { 
      code=-22;
    }
    else
    {
      vitesse=0;
      code=GroupJogParametersSet (this,1,&vitesse,&acceleration);
      if (code==0)
      {
        do 
          (GroupJogCurrentGet(this,1,&vitesse,&acc));
        while ((vitesse > 0.00001) || (vitesse<-0.00001));
        ACE_OS::sleep(t); // wait 100 ms
        code=GroupJogModeDisable (this); 
      }
    }
  }
  sprintf(origin_error,"End jog");
  test_error(code);
  return(code);
}

int XPS_axis::jog_minus()
{
  int code;
  double velocity;
  bool pret;
  code=is_ready(pret);
  if (code==0)
  {
    if (pret)
    {
      code=GroupJogModeEnable (this);
      if (code==0)
      {
        velocity=-speed;
        code=GroupJogParametersSet (this,1,&velocity,&acceleration);
      }
    }
    else
    {
      code=-22;
    }
  }
  sprintf(origin_error,"Jog");
  test_error(code);
  return(code);
}

int XPS_axis::jog_plus()
{
  int code;
  double velocity;
  bool pret;
  code=is_ready(pret);
  if (code==0)
  {
    if (pret)
    {
      code=GroupJogModeEnable (this);
      if (code==0)
      {
        velocity=speed;
        code=GroupJogParametersSet (this,1,&velocity,&acceleration);
      }
    }
    else
    {
      code=-22;
    }
  }
  sprintf(origin_error,"Jog");
  test_error(code);
  return(code);
}

int XPS_axis::stop()
{
  int code;
  int status;
  code=GroupStatusGet (this,&status);
  if (code==0)
  {
    if (status==47)
    {
      code=end_jog();
    }
    else
    {
      code=GroupMoveAbort (this);
      do
      {
        code=GroupStatusGet (this,&status);
      }
      while(status == 44 );
    }
  }

  sprintf(origin_error,"Stop");
  test_error(code);
  return(code);
}

int XPS_axis::halt()
{
  int code ;
  code=GroupKill(this);
  if (code==0)
  {
    code=GroupInitialize (this);
  }
  sprintf(origin_error,"halt");
  test_error(code);
  return(code);
}

int XPS_axis::get_soft_limit(double &plus, double &minus)
{
  int code;
  code=PositionerUserTravelLimitsGet (this,&minus,&plus); 
  sprintf(origin_error,"Get soft limits");
  test_error(code);
  return(code);
}

int XPS_axis::set_soft_limit(double plus, double minus)
{
  int code;
  code=PositionerUserTravelLimitsSet (this,minus,plus); 
  sprintf(origin_error,"Set soft limits");
  test_error(code);
  return(code); 
}

int XPS_axis::lit_version(char *version)
{
  int code;
  code=FirmwareVersionGet(this, version);
  sprintf(origin_error,"Read version");
  test_error(code);
  return(code);
}

int XPS_axis::get_positioner_error_as_string(int *error_code, char * s)
{
  int ret;
  ret = PositionerErrorGet(this, error_code);
  if (ret < 0)
  {
    sprintf(origin_error,"get_error_as_string");
    test_error(ret);
  }  
  else
  {
    ret=PositionerErrorStringGet(this, *error_code, s);
    sprintf(origin_error,"get_error_as_string");
    test_error(ret);
  }
  return(ret);
}

int XPS_axis::get_error_as_string(int code, char * s)
{
  int ret;
  ret=PositionerErrorStringGet(this,code,s);
  sprintf(origin_error,"get_error_as_string");
  test_error(ret);
  return(ret);
}

int  XPS_axis::get_status_as_string(int code, char * s)
{
  int ret;
  ret= StatusStringGet(this,code, s);
  sprintf(origin_error,"get_status_as_string");
  test_error(ret);
  return(ret);
}

//- TANGODEVIC-57:
//-------------------
int XPS_axis::init_backlash()
{
	int code;
	bool pret;
	double backlashValue = 0.;
	char strStatus[9];
	
    code = is_ready(pret);
	if (code == 0)
	{
		code = PositionerBacklashGet (this, &backlashValue, strStatus); 
		if ((code == 0) && 
		    (backlashValue != 0. )&&
			(strcmp(strStatus, "Disable") == 0))			
		{
			code = PositionerBacklashEnable(this);
		}
	}
	
	sprintf(origin_error,"Initialize backlash compensation");
	test_error(code);
	
	return(code); 
}
/*-----------------------------*/
/* XPS_axis object constructor */
/*-----------------------------*/

XPS_axis::XPS_axis(char *adresse,char *Group_name, char * Actionneur_name,ACE_Time_Value time_out)
{
  strcpy(group_name,Group_name);     //=>"GROUP"
  strcpy(positioner_name,Group_name);          //=>"GROUP"
  strcat(positioner_name,"."); //=>"GROUP."
  strcat(positioner_name,Actionneur_name);     //=>"GROUP.ACTIONNER"
  strcpy(ip_addr,adresse);

  nb_max_timeouts = 3;
  nb_timeouts = 0;
  this->m_is_connected = false;

  create_dialog();                 // create 2 sockets
  timeout=time_out; // medium value for initial time_out

}

/*-----------------------------*/
/* XPS_axis object destructor  */
/*-----------------------------*/
XPS_axis::~XPS_axis()
{
  close_dialog();// delete the two sockets
}


int XPS_axis::test_error(int code)
{
  int c;
  if (code!=0)
  {
    c=ErrorStringGet(this,code,ErrorString);
    if (c!=0)
    {
      sprintf (ErrorString,"XPS COMMUNICATION ERROR");

      throw thirdparty::Exception(std::string("COMMUNICATION_ERROR"),// reason
                                  std::string("host unreachable: unable to join XPS. Please check your wires and network config"),// description
                                  std::string("XPS_axis::create_dialog"),// origine
                                  thirdparty::ERR);//severity


    }
    throw thirdparty::Exception(std::string("COMMAND_ERROR"),// description
                                std::string(ErrorString),// reason
                                std::string(origin_error),// origine;
                                thirdparty::ERR);//severiry

    printf("ERROR : %s  (from %s) \n",ErrorString,origin_error);

  }

  return(code);
}


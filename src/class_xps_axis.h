/**
 * \file class_xps_axis.h
 * \author JPG Micro-services
 * \date 20-12-05
 */
#include "Exception.h"

#include <ace/INET_Addr.h>
#include <ace/SOCK_Stream.h>
#include <ace/SOCK_Connector.h>
#include <ace/OS.h>
#include <ace/Log_Msg.h>


/**
 * This class provides the essential commands to pilot XPS<BR> 
 *  throw the Tango framework.<BR>
 *
 * class_xps_axis uses XPS_lib as a XPS API wrapper,<BR>
 * The ACE framework to deal with sockets (crossplatform capabilities)<BR>
 * and now class_xps_axis also uses the Exception class of soleil to throw exceptions.
 *
 *----------------------------
 *-The communication concept:-
 *----------------------------
 *
 *Commands and results are encapsulated into TCP/IP packets.
 *The XPS is listening on port 5001
 *
 *We had two problems to solve:
 *
 *1)The XPS ALWAYS respond to a command BUT when this command is completly executed
 *or in case of error. It can be very long sometimes, and we had time out errors.
 *
 *2)ASynchronous / non-blocking sockets do not respond to the problem because there's 
 *another time out wich is the TANGO time out.
 *
 *Fortunatly XPS is able to manage 30 sockets at the same time. So we work around the
 *problem with two sockets: one with a very short time out to send commands and don't
 *care the response another with a long time out to send commands and treat the response.
 *We're helped by the fact that all status commands respond in a short time.
 *So when we send a command with long response delay and we need to know the state of an axis,
 *we simply poll the status. 
 *
 */
class XPS_axis
{
  private: 
    double speed;
    double acceleration;
    //- interdit la com si MC pas atteignable
    unsigned int nb_max_timeouts;
    unsigned int nb_timeouts;
    bool m_is_connected;

    /**
     * This (ACE'ed) socket is used to send some commands without waiting for any response.<BR>  
     * The XPS responds to each command at the end of the execution of the command.<BR>
     * It can be very long and causes time out problems:<BR>
     * 
     * 1) There's some solutions to handle sockets in this situation<BR>
     *    (Asynchronus communication) but...<BR>
     * 2) There's also a TANGO time out, so we decide to use this trick.
     */
    ACE_SOCK_Stream socket_send_XPS;
    /**
     * This (ACE'ed) socket is used to send some commands and waiting for the response.<BR>  
     * Very usefull if you want to read positions af an axis ;-)
     */
    ACE_SOCK_Stream socket_send_and_receive_XPS;
    /**
     * Communication time out
     */
    ACE_Time_Value timeout; 

    /**
     * This function provides all the steps to connect the client to the XPS
     */
    int create_dialog();
    /**
     * This one close the communication
     */
    int close_dialog();

  public:
    //- accessor for is_connected
    inline bool is_connected() const
    {
      return m_is_connected;
    }

    /**
     * The name of the group
     */
    char group_name[50];
    /**
     * The name of the axis in the group
     */
    char positioner_name[50];
    /**
     * The ip address of the xps
     */
    char ip_addr[50];
    /**
     * If an error occurs we want to have a semi_human description of the error  
     */
    char ErrorString[100];
    /**
     * If an error occurs we want to know from what function it comes 
     */
    char origin_error[100];

    /**
     * This function provides the version of the XPS firmware. <BR>
     * For maintenance purpose only.
     */
    int lit_version(char *version);
    /**
     * This function is here to set the communication Time Out <BR>
     * Warning: this time out must be < to the TANGO time out          
     */
    int set_timeout(ACE_Time_Value time);
    /**
     * To read the communication programmed Time Out <BR>          
     */
    int get_timeout(ACE_Time_Value &time);
    /**
     * To read the axis position <BR>          
     */ 
    int position_read(double &pos);
    /**
     * To read the PROGRAMMED speed <BR>
     * Warning: XPS has no capabilities to read the currant moving speed   
     */ 
    int get_speed(double &velocity);
    /**
     * To set the speed <BR>   
     */ 
    int set_speed(double &velocity);
    /**
     * To read the PROGRAMMED acceleration <BR>  
     */
    int get_acc(double &acc);
    /**
     * To set the acceleration <BR>  
     */
    int set_acc(double &acc);
    /**
     * This function is here to know the initialization <BR>
     * status of a group axis <BR>
     * return true if initialization done <BR>
     * or false if initialization is not done/complete <BR>
     * Note: the initialization is complete after a GroupInitialize and a GroupHomeSearch<BR>
     * The group axis will not be ready to move if homing is not done.
     */
    int is_initialized(bool &init);
    /**
     * This function is here to read if the motor is on or off <BR>
     * return true if on<BR>
     * or false if off.
     */
    int is_motor_on(bool &is_on);
    /**
     * This function is here to know if a group axis is ready to move<BR>
     * return true if ready to move<BR>
     * or false if not.
     */
    int is_ready(bool &ready);
    /**
     * This function is here to know if a group axis is moving<BR>
     * return true if moving<BR>
     * or false if not.
     */
    int is_moving(bool &is_moving);
    /**
     *  Read the End of Run status <BR>
     *  Code 0 --> No End of Run <BR>
     *  Code 1 --> End of Run (PLUS) <BR>
     *  Code 2 --> End of Run (MINUS) <BR>
     *  Code 3 --> End of Run (PLUS and MINUS) --> Check your wires
     */
    int read_eor(int &eor);
    /**
     * To read the status code of a group axis (cf XPS doc for codes)  
     */
    int read_status(int &stat);
    /**
     * To set a motor on  
     */
    int set_motor_on();
    /**
     * To set a motor off  
     */
    int set_motor_off();
    /**
     * To initialize a group axis.<BR>
     * the initialization is done after 3 steps <BR>
     * 1) We do a GroupKill because it's not authorized to re-initialize a group<BR>
     *    so, to be sure everything is ok we try to kill it before.<BR>
     * 2) Then we do a GroupInitialize<BR>
     * 3) Then we do a GroupHomeSearch, while to process is complete and the motor<BR>
     *    is ready to move only when a homing has been done
     */
    int initialization();
    
    //------------------------------------------------------------------
    //- define position :
    // to set the current axis postion to the position
    //------------------------------------------------------------------
    int define_position(double _position);
    
    
    /**
     *  absolute displacement <BR>
     *  pos --> value of the position after displacement
     */
    int move_absolute(double pos);
    /**
     *  relative displacement <BR>
     *  depl --> value of the displacement (in predefined XPS unit)
     */
    int move_relative(double depl);
    /**
     *  continuous displacement in plus
     */
    int jog_plus();
    /**
     *  continuous displacement in minus
     */
    int jog_minus();
    /**
     *  to stop the jog
     */
    int end_jog();
    /**
     *  the soft/normal way to stop a group moving<BR>
     *  with deceleration.
     */
    int stop();
    /**
     *  this is the hard way to stop a group moving or whatelse<BR>
     *  It just kill the group without any test!<BR>
     *  Use it only in case of emergency. 
     */
    int halt();
    /**
     *  To read software plus and minus limit of displacement<BR>
     */
    int get_soft_limit(double &plus, double &minus);
    /**
     *  To set software plus and minus limit of displacement<BR>
     */
    int set_soft_limit(double plus, double minus);

    /**
     *  To transform a positioner XPS command error code<BR>
     *  into a semi_human language ;-)
     */
    int get_positioner_error_as_string(int *error_code, char * s);

    /**
     *  To transform an XPS command error code<BR>
     *  into a semi_human language ;-)
     */
    int get_error_as_string(int code, char * s);
    /**
     *  To transform a group status code<BR>
     *  into a semi_human language ;-)
     */
    int get_status_as_string(int code, char * s);
    /**
     *  To send a command without waiting for any response<BR>
     */
    int send(char *message);
    /**
     *  To send a command waiting for a response<BR>
     */
    int send_and_receive(char *message, char *reponse);

    /**
     *  To capture the native error code of an XPS command, transform<BR>
     *  it in semi_human language and throw it to tango using the Exception class. 
     */
    int test_error(int code);

    /**
	 *  TANGODEVIC-57:<BR>
     *  To initialize the backlash compensation if defined on the axis<BR>
     */
    int init_backlash();
	
    /**
     *  Constructor: Takes 4 parameters. <BR> 
     *  (1) char *adresse ---> The ip adress of the xps device <BR>
     *  (2) char *Groupe_name ---> The name of the axes group you want to command <BR>
     *  (3) char * Actionneur_name ---> The name of the axis you want to command  <BR> 
     *  (4) ACE_Time_Value time_out ---> The communication time_out
     */
    XPS_axis(char *adresse,char * Group_name, char * Actionneur_name,ACE_Time_Value time_out); 

    /**
     *  Classical destructor : Nothing special
     */
    ~XPS_axis();
};

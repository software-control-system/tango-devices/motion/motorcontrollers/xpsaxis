//  Exception class
// 
// Classe d'exception utilis� dans des librairies C++ devant etre int�gr�es � des Devices TANGO
//
// $Author: stephle $
// $Revision: 1.1 $
// $Log: not supported by cvs2svn $

/*

  Lorsque la librairie doit signaler une erreur, le d�veloppeur doit " lancer " une exception compos�e des 4 champs suivants, 
  charg�s de d�finir au mieux l'erreur pour les couches logicielles sup�rieures (Interfaces op�rateurs, scripts, etc ..). 
  
	Raison d'erreur :
	=================
	Voir liste des types possibles d'erreur  ci-dessous: Ces types sont toujours � indiquer en  MAJUSCULES
	==> Objectif : aiguiller l'op�rateur vers la cause fondamentale du probl�me . Hardware, Software , Equipement d�faillant, etc 
	
	  
	Description de l'erreur :
	==========================
	Un message expliquant ce qui a caus� le probl�me
	==> Objectif : aiguiller l'expert du syst�me consid�r� vers la cause fondamentale du probl�me . Hardware, Software
		
	Origine de l'erreur : 
	====================
	Nom de la m�thode C++ qui a lanc� l'erreur . 
		  
	==> Objectif : aiguiller l'informaticien sur l'endroit de la d�faillance dans son code
			
	Severite de l'erreur
	====================
	Donne la gravit� de l'erreur. 3 valeurs possibles : 
		  . WARN (condition anormale non bloquante)
		  . ERR, valeur par d�faut (condition anormale empechant l'execution de l'action demand�e)
		  . PANIC (condition anormale empechant toute action sur le syst�me)
			  
				
				  
Exemple correct d'utilisation 
=============================
Raison de l'erreur (reason): OUT_OF_MEMORY
Description de l'erreur (desc) :	"AxisMotionAccuracy must be at least of 1 motor step !" 				
Origine de l'erreur (origin) :	"GalilAxis::write_attr_hardware"
			
Le mauvais exemple 
==============
Type d'erreur	GalilAxisNotAllowed	    ==> Message non d�fini dans la liste ci-dessous
Raison de l'erreur	Not started			    ==> Trop vague
Origine de l'erreur	Read_attributes   ==> Il manque le nom de la classe pour d�finir pr�cis�ment ce qui a g�n�r� l'erreur
						  
Liste des messages possibles pour le champ (reason) 
===================================================
							
	OUT_OF_MEMORY
	HARDWARE_FAILURE
	SOFTWARE_FAILURE
	DATA_OUT_OF_RANGE
	CONFIGURATION_ERROR
	COMMUNICATION_BROKEN
	OPERATION_NOT_ALLOWED
	DRIVER_FAILURE
	UNKNOW_ERROR
							  
Exemple de throw d'1 exception:
===============================
throw thirdparty::Exception(std::string("COMMUNICATION_ERROR"),// reason
								std::string("ACE SOCKET FATAL ERROR"),// description
								std::string("XPS_axis::create_dialog"), // origine
								thirdparty::WARN);// severity);
								

  
Remarque compl�mentaire
=======================

  La classe Exception propos�e permet en outre de garder la trace d'une pile d'erreur
  et de renvoyer vers les couches logicielles appellantes, la liste des erreurs successives. 

  Exemple d'utilisation 

  try 
  { 
  call_my_internal_function();	// may throw an Exception object
  }
  catch (thirdparty::Exception e&)
  {
	// do some local error recovery 
	blah blah ...
	// 
	// 
	e.push_errors(std::string("HARDWARE_FAILURE"),// reason
								std::string("Recovery of hardware reading error "),// description
								std::string("XPS_axis::catch function"), // origine
								thirdparty::WARN);// severity);
	rethrow(e);
}




*/
// copyleft :       Synchrotron SOLEIL
//                  L'Orme des Merisiers
//                  Saint-Aubin - BP 48
//                  91192 GIF-sur-YVETTE CEDEX
//

#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

/// ============================================================================
/// DEPENDENCIES
/// ============================================================================
#include <string>
#include <vector>

namespace thirdparty {
	
/// ============================================================================
///  Errors severities 
/// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} ErrorSeverity;

// ============================================================================
/// The Error abstraction base class.  
// ============================================================================

class Error
{
public:

  /**
   * Initialization. 
   */
  Error (void);

  /**
   * Initialization. 
   */
  Error (const char *reason,
				 const char *desc,
				 const char *origin,
	       int severity = ERR);
  

  /**
   * Initialization. 
   */
  Error (const std::string& reason,
				 const std::string& desc,
				 const std::string& origin, 
	       int severity = ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);

  /**
   * Error details: code 
   */
  virtual ~Error (void);

  /**
   * operator= 
   */
  Error& operator= (const Error& _src);

  /**
   * Error details: reason 
   */
  std::string reason;

  /**
   * Error details: description 
   */
  std::string desc;

  /**
   * Error details: origin 
   */
  std::string origin;

  /**
   * Error details: severity 
   */
  int severity;

};

/// ============================================================================
/// The error list.	
/// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
/// The Exception abstraction base class.  
// ============================================================================
//  
// detailed description to be written
// 
// ============================================================================
class Exception
{
public:

  /**
   * Initialization. 
   */
  Exception (void);

  /**
   * Initialization. 
   */
  Exception (const char *reason,
					   const char *desc,
					   const char *origin,
	           int severity = ERR);
  
  /**
   * Initialization. 
   */
  Exception (const std::string& reason,
					   const std::string& desc,
					   const std::string& origin, 
	           int severity = ERR);

  /**
   * Initialization. 
   */
  Exception (const Error& error);


  /**
   * Copy constructor. 
   */
  Exception (const Exception& src);

  /**
   * operator=
   */
  Exception& operator= (const Exception& _src); 

  /**
   * Release resources.
   */
  virtual ~Exception (void);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
					         const char *desc,
						       const char *origin, 
		               int severity = ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
                   const std::string& desc,
                   const std::string& origin, 
                   int severity = ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);

  /**
   * The errors list
   */
   ErrorList errors;
    
private:

};

}	//end namespace
#endif /// _EXCEPTION_H_


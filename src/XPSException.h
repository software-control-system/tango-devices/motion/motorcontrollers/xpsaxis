// XPSException class

/*

Lorsque la librairie doit signaler une erreur, le d�veloppeur doit " lancer " une exception compos�e des 3 champs suivants, 
charg�s de d�finir au mieux l'erreur pour les couches logicielles sup�rieures (Interfaces op�rateurs, scripts, etc ..). 

Raison d'erreur :
=================
  Voir liste des types possibles d'erreur  ci-dessous: Ces types sont toujours � indiquer en  MAJUSCULES
	==> Objectif : aiguiller l'op�rateur vers la cause fondamentale du probl�me . Hardware, Software , Equipement d�faillant, etc 

	
Description de l'erreur : 
==========================
Un message expliquant ce qui a caus� le probl�me
		==> Objectif : aiguiller l'expert du syst�me consid�r� vers la cause fondamentale du probl�me . Hardware, Software

Origine de l'erreur : 
====================
Nom de la m�thode C++ qui a lanc� l'erreur . 

	  ==> Objectif : aiguiller l'informaticien sur l'endroit de la d�faillance dans son code

Le bon exemple 
===============
Raison de l'erreur (reason): OUT_OF_MEMORY
Description de l'erreur (desc) :	"AxisMotionAccuracy must be at least of 1 motor step !"					
Origine de l'erreur (origin) :	"GalilAxis::write_attr_hardware"

Le mauvais exemple 
==============
Type d'erreur 	GalilAxisNotAllowed	    ==> Message non d�fini dans la liste ci-dessous
Raison de l'erreur 	Not started			    ==> Trop vague
Origine de l'erreur 	Read_attributes	  ==> Il manque le nom de la classe pour d�finir pr�cis�ment ce qui a g�n�r� l'erreur



Liste des messages possibles pour le champ (reason) 
===================================================

OUT_OF_MEMORY
HARDWARE_FAILURE
SOFTWARE_FAILURE
DATA_OUT_OF_RANGE
CONFIGURATION_ERROR
COMMUNICATION_BROKEN
OPERATION_NOT_ALLOWED
DRIVER_FAILURE
UNKNOW_ERROR

exemple de throw :
==================
throw XPSException(std::string("COMMUNICATION_ERROR"),// reason
			             std::string("ACE SOCKET FATAL ERROR"),// description
			             std::string("XPS_axis::create_dialog"));// origine);


*/



#ifndef __XPSException_class
#define __XPSException_class

#include <string>

class XPSException
{
 public:
	 XPSException ( std::string r, std::string d, std::string o ) : _reason ( r ),
																	_origin ( o ),
																	_desc   ( d ){};
  ~XPSException (){};

  std::string reason()      { return _reason; }
  std::string description() { return _desc; }
  std::string origin()      { return _origin; }
 
 private:

  std::string _reason;		
  std::string _origin;
  std::string _desc;

};

#endif //__XPSException_class


#include <ace/INET_Addr.h>
#include <ace/SOCK_Stream.h>
#include <ace/SOCK_Connector.h>
#include <ace/OS.h>
#include <ace/Log_Msg.h>

#define SIZE_BUFFER  512

/**
 * This is the library to command the XPS (a wrapper of teh XPS API) 
 * a little bit commented but you have to refer to the XPS manual to have more details.
 */

class XPS_axis;

/**
 * This command provides the version of the XPS firmware. <BR>
 * For maintenance purpose only.
 */
int FirmwareVersionGet (XPS_axis * axis, char * Version);

/**
 * Activates the selected home search, configured in the Stages.ini, <BR>
 * for the selected group.
 */
int GroupHomeSearch (XPS_axis * axis) ;

/** This allows to change on fly the velocity and the acceleration used by the JOG <BR>
 *  mode. If an error is occurred, the positioner and all axes from the same group are <BR>
 *  stopped with a velocity NULL. <BR>
 *  Note: The JOG mode must be activated (after the call of GroupJogModeEnable)
 */
int GroupJogParametersSet (XPS_axis * axis, int NbElements, double Velocity[], double Acceleration[]);

/** This returns the user velocity and the user acceleration changed with the<BR>
 *  GroupJogParametersSet.
 */
int GroupJogCurrentGet (XPS_axis * axis, int NbElements, double Velocity[], double Acceleration[]); 

/**The group must be in ready status (after initialization and home search) and if OK <BR>
 *the group is setting in JOG mode (JOGGING status).
 */
int GroupJogModeEnable (XPS_axis * axis);

/**Disable the Jog mode : Exit JOGGING status to come back in READY status.<BR>
 *The group must be in JOGGING status and the positioner must be idle (velocity <BR>
 *must be NULL).
 */
int GroupJogModeDisable (XPS_axis * axis);

/**
 * Kills and resets the group. The group comes back to the not initialized status.
 */
int GroupKill (XPS_axis * axis);

/**
 * Initialize the motors and activate the servo loop of each positioner of the selected group.
 */ 
int GroupInitialize (XPS_axis * axis);


//------------------------------------------------------------------------------------------------------------------------------------
/**
 * 3 following methods are usefull to define the position of the axis
 * (to set the axis current postion at ReferencingParameter)
 */ 
int GroupReferencingStart (XPS_axis * axis) ;

int GroupReferencingActionExecute (XPS_axis * axis, char * ReferencingAction, char * ReferencingSensor, double ReferencingParameter);

int GroupReferencingStop (XPS_axis * axis);
//------------------------------------------------------------------------------------------------------------------------------------

/**Aborts motion : stops the motion in progress on the selected group.
 */
int GroupMoveAbort (XPS_axis * axis); 

/**The selected positioner moves to the target position.<BR>
 *The absolute move refers to the acceleration, velocity, minimumTjerkTime and <BR>
 *maximumTjerkTime predefined in the Stages.ini or redefined with the <BR>
 *PositionerSGammaParametersSet command.
 */
int GroupMoveAbsolute (XPS_axis * axis,int NbElements, double TargetPosition[]);

/**The selected positioner moves to the target position<BR> :
 *TargetPosition = CurrentPosition + TargetDisplacement<BR>
 *The relative move refers to the acceleration, velocity, minimumTjerkTime and<BR>
 *maximumTjerkTime predefined in the Stages.ini or redefined with the<BR>
 *PositionerSGammaParametersSet.
 */
int GroupMoveRelative (XPS_axis * axis,int NbElements, double TargetDisplacement[]);

/**Turn motor OFF and set the MotionEnable status to FALSE for the selected SingleAxis.
 */
int GroupMotionDisable (XPS_axis * axis);

/**Turn motor ON and set the MotionEnable status to TRUE for the selected SingleAxis.
 */ 
int GroupMotionEnable (XPS_axis * axis);

/**Reads the SingleAxis current position
 */ 
int GroupPositionCurrentGet (XPS_axis * axis,int NbElements, double CurrentEncoderPosition[]) ;

/**Reads SingleAxis group status and returns the group status code.<BR>
 *Call the GroupStatusStringGet API to get the group status description from the group status code.<BR>
 *in a human-like language ;-)
 */
int GroupStatusGet (XPS_axis * axis, int * Status);

/**Returns the group status string corresponding to the group status code
 */ 
int GroupStatusStringGet (XPS_axis * axis,int GroupStatusCode, char * GroupStatusString);

/**A 3D shooter video game?<BR>
 *No:Kills and resets all groups. All groups come back to the not initialised status. 
 */ 
int KillAll (XPS_axis * axis);

/**Returns the positioner error code and clears the positioner error. To get the error<BR>
 *code description, call the PositionerErrorStringGet command.<BR>
 *The positioner error is composed of the corrector error, the profile generator error<BR>
 *and the servitudes error.
 */
int PositionerErrorGet (XPS_axis * axis, int * ErrorCode);

/**Returns the positioner error description in relation to a positioner error code, in a simili-human way
 */ 
int PositionerErrorStringGet (XPS_axis * axis,int PositionerErrorCode, char * PositionerErrorString);

/**Returns the positioner hardware status<BR>
 *Positioner hardware status is composed of corrector and servitudes hardware status:<BR>
 *PositionerHardwareStatus = CorrectorHardwareStatus or ServitudesHardwareStatus <BR>
 *or INTGlobalServitudesHardwareStatus
 */
int PositionerHardwareStatusGet (XPS_axis * axis,int * HardwareStatus); 

/**Returns the positioner hardware status description in relation to a positioner<BR>
 *hardware status
 */
int PositionerHardwareStatusStringGet (XPS_axis * axis,int PositionerHardwareStatus, char * PositonerHardwareStatusString);

/**Read user travel limits:<BR>
 *UserMinimumTargetPosition and UserMaximumTargetPosition.
 */
int PositionerUserTravelLimitsGet (XPS_axis * axis, double * UserMinimumTarget, double * UserMaximumTarget);

/**Updates user travel limits' parameters and saves travel limits parameters:<BR>
 *UserMinimumTargetPosition = NewUserMinimumTargetPosition;<BR>
 *UserMaximumTargetPosition = NewUserMaximumTargetPosition;
 */
int PositionerUserTravelLimitsSet (XPS_axis * axis, double UserMinimumTarget, double UserMaximumTarget);

/** Read the list of error codes of an axis
 */  
int ErrorListGet (XPS_axis * axis,char * ErrorsList);

/**Read the list of error codes of a group
 */ 
int GroupStatusListGet (XPS_axis * axis,char * GroupStatusList);

/**Returns the dynamic parameters of the positioner for a future motion:<BR>
 *UserVelocity, UserAcceleration, UserMinimumJerkTime and<BR>
 *UserMaximumJerkTime.
 */
int PositionerSGammaParametersGet (XPS_axis * axis, double * Velocity, double * Acceleration, double * MinimumTjerkTime, double * MaximumTjerkTime);

/**Updates positioner dynamic parameters for a future displacement:<BR>
 *UserVelocity = NewVelocity<BR>
 *UserAcceleration = NewAcceleration<BR>
 *UserMinimumJerkTime = NewMinimumJerkTime<BR>
 *UserMaximumJerkTime = NewMaximumJerkTime
 */
int PositionerSGammaParametersSet (XPS_axis * axis, double Velocity, double Acceleration, double MinimumTjerkTime, double MaximumTjerkTime); 

/**
 * Give the code of an error to the XPS and get an almost_human traduction in english 
 */
int ErrorStringGet (XPS_axis * axis, int ErrorCode, char * ErrorString);

/**Returns the positioner hardware status description (in a not_so_far_away_from_human_blabla)<BR>
 *in relation to a positioner hardware status code
 */
int StatusStringGet (XPS_axis * axis, int StatusCode, char * StatusString);

//- Absolute move of a positioner even if it is part of a Group.
int PositionerMoveAbsolute (XPS_axis * axis, double TargetPosition);


//------------------------------------------------------------------------------------------------------------------------------------
/**
 * 2 following methods are usefull to activate the backlash compensation,
*  if it's defined on the axis
 */ 

int PositionerBacklashGet (XPS_axis * axis, double * BacklashValue, char *Statuschar );

int PositionerBacklashEnable (XPS_axis * axis );

//------------------------------------------------------------------------------------------------------------------------------------

//  Exception class
// 
// Classe d'exception utilis� dans des librairies C++ devant etre int�gr�es � des Devices TANGO
//
// $Author: stephle $
// $Revision: 1.1 $
// $Log: not supported by cvs2svn $
//
// copyleft :       Synchrotron SOLEIL
//                  L'Orme des Merisiers
//                  Saint-Aubin - BP 48
//                  91192 GIF-sur-YVETTE CEDEX
//-======================================================================
// ============================================================================
# include "Exception.h"

namespace thirdparty
{
// ============================================================================
// Error::Error
// ============================================================================
Error::Error (void)
  :  reason ("unknown"),
     desc ("unknown error"),
     origin ("unknown"),
     severity (ERR)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const char *_reason,
              const char *_desc,
              const char *_origin,
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const std::string& _reason,
              const std::string& _desc,
              const std::string& _origin,
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const Error& _src)
  :  reason (_src.reason),
     desc (_src.desc),
     origin (_src.origin),
     severity (_src.severity)
{

}

// ============================================================================
// Error::~Error
// ============================================================================
Error::~Error (void)
{

}

// ============================================================================
// Error::operator=
// ============================================================================
Error& Error::operator= (const Error& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->reason = _src.reason;
  this->desc = _src.desc;
  this->origin = _src.origin;
  this->severity = _src.severity;

  return *this;
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (void) 
  : errors(0)
{
  this->push_error(Error());
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const char *_reason,
                      const char *_desc,
                      const char *_origin,
                      int _severity) 
  : errors(0)
{
  this->push_error(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const std::string& _reason,
                      const std::string& _desc,
                      const std::string& _origin,
                      int _severity) 
  : errors(0)
{
  this->push_error(_reason, _desc, _origin, _severity);
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const Exception& _src) 
  : errors(0)
{
  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception& Exception::operator= (const Exception& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->errors.clear();

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }

  return *this;
}

// ============================================================================
// Exception::~Exception
// ============================================================================
Exception::~Exception (void)
{
  this->errors.clear();
}


// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const char *_reason,
                            const char *_desc,
                            const char *_origin, 
                            int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const std::string& _reason,
                            const std::string& _desc,
                            const std::string& _origin, 
                            int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const Error& _error)
{
  this->errors.push_back(_error);
}

} //end namespace

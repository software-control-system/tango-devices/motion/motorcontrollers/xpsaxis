#include <stdio.h> 
#include <stdlib.h> 
#include <stdarg.h> 
#include <string.h>

#include "XPS_lib.h" 
#include "class_xps_axis.h"

/*------------------------------------------------*/
/* Function to get the version of the XPS firmware*/
/*------------------------------------------------*/

int FirmwareVersionGet (XPS_axis * axis, char * Version)
{ 
  int ret = -1;//return value of the fuction 
  char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 
  sprintf (ExecuteMethod, "FirmwareVersionGet (char *)");//now ExecuteMethod is initialized
  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket
  //because we want to treat the answer 
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 
  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator (,) the first value is the error code
    //here it's 0, so we don't care
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (Version, pt);//get the version number
    ptNext = strchr (Version, ',');
    if (ptNext != NULL) *ptNext = '\0';//end of string
  } 
  return (ret);//return the error code 
}


/*------------------------------------------------*/
/*         Function for Homing                    */
/*------------------------------------------------*/
int GroupHomeSearch (XPS_axis * axis) 
{ 
  ACE_Time_Value t (0,10000);//t=100 ms
  char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
  sprintf (ExecuteMethod, "GroupHomeSearch (%s)", axis->group_name);//now ExecuteMethod is initialized
  axis->send(ExecuteMethod);//send the packet
  ACE_OS::sleep(t);//wait 100 ms
  return (0); 
}

/*------------------------------------------------*/
/*   Function for initializing the Group          */
/*------------------------------------------------*/
int GroupInitialize (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction 
  char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 
  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupInitialize (%s)", axis->group_name);//now ExecuteMethod is initialized
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret);//get the return code 
  return (ret); 
}

/*---------------------------------------------------*/
/*   Function for start a referencing on the Group   */
/*---------------------------------------------------*/
int GroupReferencingStart (XPS_axis * axis) 
{
	int ret = -1; 
	char ExecuteMethod [SIZE_BUFFER]; 
	char ReturnedValue [SIZE_BUFFER]; 

	/* Convert to string */ 
	sprintf (ExecuteMethod, "GroupReferencingStart (%s)", axis->group_name);

	/* Send this string and wait return function from controller */ 
	/* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
	if (strlen (ReturnedValue) > 0) 
		sscanf (ReturnedValue, "%i", &ret); 

	/* Get the returned values in the out parameters */ 
	return (ret); 
}


/*-----------------------------------------------------*/
/*   Function for execute a referencing on the Group   */
/*-----------------------------------------------------*/
int GroupReferencingActionExecute (XPS_axis * axis, char * ReferencingAction, char * ReferencingSensor, double ReferencingParameter) 
{ 
	int ret = -1; 
	char ExecuteMethod [SIZE_BUFFER]; 
	char ReturnedValue [SIZE_BUFFER]; 

	/* Convert to string */ 
	sprintf (ExecuteMethod, "GroupReferencingActionExecute (%s,%s,%s,%.13g)", axis->positioner_name, ReferencingAction, ReferencingSensor, ReferencingParameter);

	/* Send this string and wait return function from controller */ 
	/* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
	if (strlen (ReturnedValue) > 0) 
		sscanf (ReturnedValue, "%i", &ret); 

	/* Get the returned values in the out parameters */ 
	return (ret); 
}


/*---------------------------------------------------*/
/*   Function for stop a referencing on the Group    */
/*---------------------------------------------------*/
int GroupReferencingStop (XPS_axis * axis) 
{ 
	int ret = -1; 
	char ExecuteMethod [SIZE_BUFFER]; 
	char ReturnedValue [SIZE_BUFFER]; 

	/* Convert to string */ 
	sprintf (ExecuteMethod, "GroupReferencingStop (%s)",  axis->group_name);

	/* Send this string and wait return function from controller */ 
	/* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
	if (strlen (ReturnedValue) > 0) 
		sscanf (ReturnedValue, "%i", &ret); 

	/* Get the returned values in the out parameters */ 
	return (ret); 
}

/*------------------------------------------------*/
/*       Function to set jog prarmeters           */
/*------------------------------------------------*/
int GroupJogParametersSet (XPS_axis * axis, int NbElements, double Velocity[], double Acceleration[]) 
{ 
  char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
  char temp[SIZE_BUFFER];
  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupJogParametersSet (%s,", axis->group_name);
  for (int i = 0; i < NbElements; i++)//prepare velocity and acceleration array i=number of axes in the group
  {
    sprintf (temp, "%lf,%lf", Velocity[i], Acceleration[i]);
    strcat (ExecuteMethod, temp);//add the velocity ans acceleration for the axis number i
    if ((i + 1) < NbElements) 
    {
      strcat (ExecuteMethod, ",");//add a separator
    }
  }
  strcat (ExecuteMethod, ")");//now ExecuteMethod is initialized
  axis->send(ExecuteMethod); //Our "send" socket because we want do not need to treat the answer
  return (0);  
}

/*------------------------------------------------*/
/*       Function to get jog  current prarmeters  */
/*------------------------------------------------*/
int GroupJogCurrentGet (XPS_axis * axis, int NbElements, double Velocity[], double Acceleration[]) 
{ 
  int ret = -1; 
  char ExecuteMethod [SIZE_BUFFER];//command 
  char ReturnedValue [SIZE_BUFFER];//result 
  char temp[SIZE_BUFFER];//to play with strcat

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupJogCurrentGet (%s,", axis->group_name);
  for (int i = 0; i < NbElements; i++)
  {
    sprintf (temp, "double *,double *");
    strcat (ExecuteMethod, temp);
    if ((i + 1) < NbElements) 
    {
      strcat (ExecuteMethod, ",");
    }
  }
  strcat (ExecuteMethod, ")");

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive (ExecuteMethod, ReturnedValue); 
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;

    for (int i = 0; i < NbElements; i++)
    {
      if (pt != NULL) pt = strchr (pt, ',');
      if (pt != NULL) pt++;
      if (pt != NULL) sscanf (pt, "%lf", &Velocity[i]);
      if (pt != NULL) pt = strchr (pt, ',');
      if (pt != NULL) pt++;
      if (pt != NULL) sscanf (pt, "%lf", &Acceleration[i]);
    }
  } 
  return (ret); 
}



/*-------------------------------------------------*/
/*       Function to set the jog mode on           */
/*-------------------------------------------------*/
int GroupJogModeEnable (XPS_axis * axis) 
{ 
  int ret = -1;//return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupJogModeEnable (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 
  /* Get the returned values in the out parameters */ 
  return (ret); 
}



/*--------------------------------------------------*/
/*       Function to set the jog mode off           */
/*--------------------------------------------------*/
int GroupJogModeDisable (XPS_axis * axis) 
{ 
  int ret = -1;//return value of the fuction 
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupJogModeDisable (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}



/*---------------------------------------------------------*/
/*       Function to kill (uninitialize) a Group           */
/*---------------------------------------------------------*/
int GroupKill (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupKill (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}


/*-----------------------------------------------------------------------*/
/*       Function to abort a move without losing initialization          */
/*-----------------------------------------------------------------------*/
int GroupMoveAbort (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS


  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMoveAbort (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */

  // We commented the next line because it works better with a send and receive method. For now we did not face out a Tango time out.
  //axis->send(ExecuteMethod); //Our "send" socket because we don't want to treat the answer
  //to avoid Tango time out

  char ReturnedValue [SIZE_BUFFER]; 
  axis->send_and_receive(ExecuteMethod, ReturnedValue);
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  return (ret); 
}


/*-------------------------------------------------*/
/*       Function to do an abolute move            */
/*-------------------------------------------------*/
int GroupMoveAbsolute (XPS_axis * axis, int NbElements, double TargetPosition[]) 
{ 
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char temp[SIZE_BUFFER];

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMoveAbsolute (%s,", axis->group_name);
  for (int i = 0; i < NbElements; i++)
  {
    sprintf (temp, "%lf", TargetPosition[i]);
    strcat (ExecuteMethod, temp);
    if ((i + 1) < NbElements) 
    {
      strcat (ExecuteMethod, ",");
    }
  }
  strcat (ExecuteMethod, ")");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function not tested */ 
  axis->send(ExecuteMethod);//this time we use a send only because we don't want to treat the answer while
  //we don't know when this answer will come back.
  return (0); 
}

/*-------------------------------------------------*/
/*       Function to do an abolute move            */
/*-------------------------------------------------*/
int PositionerMoveAbsolute (XPS_axis * axis, double TargetPosition) 
{ 
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMoveAbsolute (%s, %f)", axis->positioner_name, TargetPosition);

  /* Send this string and wait return function from controller */ 
  /* return function not tested */ 
  axis->send(ExecuteMethod);//this time we use a send only because we don't want to treat the answer while
  //we don't know when this answer will come back.
  return (0); 
}

/*-------------------------------------------------*/
/*       Function to do a relative move            */
/*-------------------------------------------------*/
int GroupMoveRelative (XPS_axis * axis, int NbElements, double TargetDisplacement[]) 
{ 
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char temp[SIZE_BUFFER];

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMoveRelative (%s,", axis->group_name);
  for (int i = 0; i < NbElements; i++)
  {
    sprintf (temp, "%lf", TargetDisplacement[i]);
    strcat (ExecuteMethod, temp);
    if ((i + 1) < NbElements) 
    {
      strcat (ExecuteMethod, ",");
    }
  }
  strcat (ExecuteMethod, ")");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function not tested*/ 
  axis->send(ExecuteMethod);//this time we use a send only because we don't want to treat the answer while
  return (0); 
}


int GroupMotionDisable (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMotionDisable (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}


int GroupMotionEnable (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupMotionEnable (%s)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}

/*-------------------------------------------------*/
/*       Function to get current positions         */
/*-------------------------------------------------*/
int GroupPositionCurrentGet (XPS_axis * axis, int NbElements, double CurrentEncoderPosition[]) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 
  char temp[SIZE_BUFFER];

  // DEBUG : pour des groupes a plusieurs axes ca ne marche pas : il faudrait passer autant de ""double" que d'axes dans le groupe
  // TEMPORAIRE : recuperer uniquement la bonne valeur en utilisant le nom d'axe .

  /* Convert to string */ 
  // sprintf (ExecuteMethod, "GroupPositionCurrentGet (%s,", axis->group_name);
  sprintf (ExecuteMethod, "GroupPositionCurrentGet (%s,",axis->positioner_name);

  for (int i = 0; i < NbElements; i++)
  {
    sprintf (temp, "double *");
    strcat (ExecuteMethod, temp);
    if ((i + 1) < NbElements) 
    {
      strcat (ExecuteMethod, ",");
    }
  }
  strcat (ExecuteMethod, ")");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret);//get the return code 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;

    for (int i = 0; i < NbElements; i++)
    {
      if (pt != NULL) pt = strchr (pt, ',');//go to the next separator
      if (pt != NULL) pt++;
      if (pt != NULL) sscanf (pt, "%lf", &CurrentEncoderPosition[i]);//get the Current Encoder Position of the axis
      //number i
    }
  } 
  return (ret); 
}

/*-------------------------------------------------*/
/*       Function to get the group status          */
/*-------------------------------------------------*/
int GroupStatusGet (XPS_axis * axis, int * Status) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupStatusGet (%s,int *)", axis->group_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%d", Status);
  } 
  return (ret); 
}

/*----------------------------------------------------------------------------------*/
/*       Function to convert the status code into an almost-human string            */
/*----------------------------------------------------------------------------------*/
int StatusStringGet (XPS_axis * axis,int GroupStatusCode, char * GroupStatusString) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupStatusStringGet (%d,char *)", GroupStatusCode);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (GroupStatusString, pt);
    ptNext = strchr (GroupStatusString, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}

/*----------------------------------------------------------------------------------*/
/*       Want to uninitialize everybody? kill'em all!                               */
/*----------------------------------------------------------------------------------*/
int KillAll (XPS_axis * axis) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "KillAll ()");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}

/*----------------------------------------------------------------------------------*/
/*           Get the last error code of a positionner                               */
/*----------------------------------------------------------------------------------*/
int PositionerErrorGet (XPS_axis * axis, int * ErrorCode) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerErrorGet (%s,int *)",axis->positioner_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%d", ErrorCode);
  } 
  return (ret); 
}

/*----------------------------------------------------------------------------------*/
/*       Function to convert the error code into an almost-human string             */
/*----------------------------------------------------------------------------------*/
int PositionerErrorStringGet (XPS_axis * axis,int PositionerErrorCode, char * PositionerErrorString) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerErrorStringGet (%d,char *)", PositionerErrorCode);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (PositionerErrorString, pt);
    ptNext = strchr (PositionerErrorString, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}


int PositionerHardwareStatusGet (XPS_axis * axis, int * HardwareStatus) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerHardwareStatusGet (%s,int *)", axis->positioner_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%d", HardwareStatus);
  } 
  return (ret); 
}


int PositionerHardwareStatusStringGet (XPS_axis * axis,int PositionerHardwareStatus, char * PositonerHardwareStatusString) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerHardwareStatusStringGet (%d,char *)", PositionerHardwareStatus);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (PositonerHardwareStatusString, pt);
    ptNext = strchr (PositonerHardwareStatusString, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}

int PositionerUserTravelLimitsGet (XPS_axis * axis, double * UserMinimumTarget, double * UserMaximumTarget) 
{ 
  int ret = -1; //return value of the function
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerUserTravelLimitsGet (%s,double *,double *)", axis->positioner_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", UserMinimumTarget);
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", UserMaximumTarget);
  } 
  return (ret); 
}


int PositionerUserTravelLimitsSet (XPS_axis * axis, double UserMinimumTarget, double UserMaximumTarget) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerUserTravelLimitsSet (%s,%lf,%lf)", axis->positioner_name, UserMinimumTarget, UserMaximumTarget);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}


int ErrorListGet (XPS_axis * axis,char * ErrorsList) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "ErrorListGet (char *)");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (ErrorsList, pt);
    ptNext = strchr (ErrorsList, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}


int GroupStatusListGet (XPS_axis * axis,char * GroupStatusList) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "GroupStatusListGet (char *)");//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator (,) the first value is the error code
    //here it's 0, so we don't care
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (GroupStatusList, pt);//get the list
    ptNext = strchr (GroupStatusList, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}


int PositionerSGammaParametersGet (XPS_axis * axis, double * Velocity, double * Acceleration, double * MinimumTjerkTime, double * MaximumTjerkTime) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerSGammaParametersGet (%s,double *,double *,double *,double *)", axis->positioner_name);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;

    pt = ReturnedValue;
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator (,) the first value is the error code
    //here it's 0, so we don't care
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", Velocity);//get the velocity
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", Acceleration);//get the acceleration
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", MinimumTjerkTime);//get the MinimumTjerkTime
    if (pt != NULL) pt = strchr (pt, ',');//go to the next separator
    if (pt != NULL) pt++;
    if (pt != NULL) sscanf (pt, "%lf", MaximumTjerkTime);//get the MaximumTjerkTime
  } 
  return (ret); 
}


int PositionerSGammaParametersSet (XPS_axis * axis, double Velocity, double Acceleration, double MinimumTjerkTime, double MaximumTjerkTime) 
{ 
  int ret = -1; //return value of the fuction
  char ExecuteMethod [SIZE_BUFFER]; //Will contain the string to send to XPS
  char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "PositionerSGammaParametersSet (%s,%lf,%lf,%lf,%lf)", axis->positioner_name, Velocity, Acceleration, MinimumTjerkTime, MaximumTjerkTime);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket because we want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret); 

  /* Get the returned values in the out parameters */ 
  return (ret); 
}


int ErrorStringGet (XPS_axis * axis, int ErrorCode, char * ErrorString) 
{ 
  int ret = -1; 
  char ExecuteMethod [SIZE_BUFFER];//the command 
  char ReturnedValue [SIZE_BUFFER];//the result 

  /* Convert to string */ 
  sprintf (ExecuteMethod, "ErrorStringGet (%d,char *)", ErrorCode);//now ExecuteMethod is initialized

  /* Send this string and wait return function from controller */ 
  /* return function : ==0 -> OK ; < 0 -> NOK */ 
  axis->send_and_receive(ExecuteMethod, ReturnedValue);//send and receive --> We want to treat the answer
  if (strlen (ReturnedValue) > 0) 
    sscanf (ReturnedValue, "%i", &ret);//if the string is not empty get the return code 

  /* Get the returned values in the out parameters */ 
  if (ret == 0) 
  { 
    char * pt;
    char * ptNext;

    pt = ReturnedValue;
    ptNext = NULL;
    if (pt != NULL) pt = strchr (pt, ',');//the separator is a ","
    if (pt != NULL) pt++;
    if (pt != NULL) strcpy (ErrorString, pt);
    ptNext = strchr (ErrorString, ',');
    if (ptNext != NULL) *ptNext = '\0';
  } 
  return (ret); 
}

//- TANGODEVIC-57:Utilisation du backlash sur les XPS
//---------------------------------------------------
/*---------------------------------------------------*/
/* Function to get the current value of the backlash */
/* and the status 'Enabled' or 'Disabled'            */  
/*---------------------------------------------------*/

int PositionerBacklashGet (XPS_axis * axis, double * BacklashValue, char *Statuschar )
{ 
	int ret = -1;//return value of the fuction 

	char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
	char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 
	
	// Convert to string  
	sprintf (ExecuteMethod, "PositionerBacklashGet (%s, double *, char *)",axis->positioner_name );//now ExecuteMethod is initialized
	
	// Send this string and wait return function from controller 
	// return function : ==0 -> OK ; < 0 -> NOK 
	axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket
	
	//because we want to treat the answer 
	if (strlen (ReturnedValue) > 0) 
		sscanf (ReturnedValue, "%i", &ret); 
		
	// Get the returned values in the out parameters
	if (ret == 0) 
	{ 
		char * pt;
		char * ptNext;

		pt = ReturnedValue;
		ptNext = NULL;
		if (pt != NULL) pt = strchr (pt, ',');//go to the next separator (,) the first value is the error code
		
		//here it's 0, so we don't care
		if (pt != NULL) pt++;
		if (pt != NULL) sscanf (pt, "%lf", BacklashValue);//get the backlash compensation
		if (pt != NULL) pt = strchr (pt, ',');//go to the next separator
		if (pt != NULL) pt++;		
		if (pt != NULL) strcpy (Statuschar, pt);//get the status
		ptNext = strchr (Statuschar, ',');
		if (ptNext != NULL) *ptNext = '\0';//end of string
	} 
	return (ret);//return the error code 
}

/*-----------------------------------------------------------*/
/* Function to enable the backlash compensation on the axis  */
/*-----------------------------------------------------------*/
int PositionerBacklashEnable (XPS_axis * axis )
{
	int ret = -1;
	
	char ExecuteMethod [SIZE_BUFFER];//Will contain the string to send to XPS 
	char ReturnedValue [SIZE_BUFFER]; //Will contain the string received from the XPS 
	
	sprintf (ExecuteMethod, "PositionerBacklashEnable (%s)", axis->group_name);
	// Send this string and wait return function from controller 
	// return function : ==0 -> OK ; < 0 -> NOK 
	axis->send_and_receive(ExecuteMethod,ReturnedValue); //Our "send_and_receive" socket
	
	//read the error status of the function
	if (strlen (ReturnedValue) > 0) 
		sscanf (ReturnedValue, "%i", &ret); 
	return (ret);
}

